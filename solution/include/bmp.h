#include "image.h"
#include <stdio.h>

/*  deserializer   */
enum read_status {
    BMP_READ_OK = 0,
    BMP_READ_NULL_IMAGE,
    BMP_READ_INVALID_HEADER,
    BMP_READ_INVALID_SIGNATURE,
    BMP_READ_UNSUPPORTED_BIT_COUNT,
    BMP_READ_INVALID_BITS,
    BMP_READ_MEMORY_FILLED,
    FILE_READ_ERROR
};
/*  serializer   */
enum write_status {
    BMP_WRITE_OK = 0,
    BMP_WRITE_NULL_IMAGE,
    BMP_WRITE_ERROR,
    BMP_WRITE_TOO_BIG,
    BMP_WRITE_INVALID_BITS
};

enum read_status from_bmp( FILE* const in, struct image * const img );
enum write_status to_bmp( FILE* const out, struct image * const img );
char * bmp_write_status(const enum write_status status);
char * bmp_read_status(const enum read_status status);
