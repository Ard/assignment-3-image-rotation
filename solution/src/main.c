#include "bmp.h"
#include "file.h"
#include "image.h"
#include "rotate.h"
#include <stdio.h>

int main( int argc, char** argv ) {
     if (argc != 3) {
        fprintf(stderr, "Something is wrong with the entered arguments\n");
        return 1;
    }

    struct image img;
    FILE *in = NULL;
	FILE *out = NULL;

	if (!open_file(&in, argv[1], "rb+")) {
		fprintf(stderr, "Can't open read file\n");
		return 1;
	}

	if (!open_file(&out, argv[2], "wb+")) {
        close_file(in);
		fprintf(stderr, "Can't open write file\n");
		return 1;
	}

    const enum read_status statusFrom = from_bmp(in, &img);
    if(statusFrom != 0) {
        fprintf(stderr, "%s\n", bmp_read_status(statusFrom));
        return statusFrom;
    }

    struct image new_img = rotate(img);

    const enum write_status statusTo = to_bmp(out, &new_img);
    if(statusTo != 0) {
        fprintf(stderr, "%s\n", bmp_write_status(statusTo));
        return statusTo;
    }
    
    delete_image(&img);
    delete_image(&new_img);

    if (close_file(in) != 1) {
        fprintf(stderr, "Can't close read file");
        return 1;
    }

    if (close_file(out) != 1) {
        fprintf(stderr, "Can't close write file");
        return 1;
    }

    return 0;
}
