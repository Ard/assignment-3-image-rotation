#include "bmp.h"
#include "image.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


#define SIGNATURE 0x4D42
#define BITCOUNT 24

char * bmp_write_status(const enum write_status status) {
    char * message[5] = {
        "Succesful write",
        "BMP write error: picture is NULL",
        "BMP write error: cannot write",
        "BMP write error: image to big",
        "BMP write error: invalid bits"
    };
    if (status >=4) {
        return "BMP write error";
    } else {
        return message[status];
    }
}

char * bmp_read_status(const enum read_status status) {
    char * message[8] = {
        "Succesful read",
        "BMP read error: picture is NULL",
        "BMP read error: invalid header",
        "BMP read error: invalid signature",
        "BMP read error: unsupported type of bmp",
        "BMP read error: invalid bits",
        "BMP read error: memory filled",
        "BMP read error: file cannot be read"
    };
    if (status >=7) {
        return "BMP read error";
    } else {
        return message[status];
    }
}


static size_t calculatePadding(const uint64_t width) {
    return 4 - width * sizeof(struct pixel) % 4;
}

enum read_status from_bmp(FILE* const in, struct image* const img ) {
    if (in == NULL) {
        return FILE_READ_ERROR;
    }
    struct bmp_header header;
    if (!img) return BMP_READ_NULL_IMAGE;
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return BMP_READ_INVALID_HEADER;
    }
    if (header.bfType != SIGNATURE) {
        return BMP_READ_INVALID_SIGNATURE;
    }

    if(header.biBitCount != BITCOUNT) {
        return BMP_READ_UNSUPPORTED_BIT_COUNT;
    }
    if(fseek(in, (long) header.bfOffBits, SEEK_SET) != 0) {
        return BMP_READ_INVALID_BITS;
    }

    img->width = header.biWidth;
    img->height = header.biHeight;

    img->data = (struct pixel*) malloc(sizeof(struct pixel) * header.biWidth * header.biHeight);
    if(!img->data) {
        return BMP_READ_MEMORY_FILLED;
    }

    const uint8_t padding = (uint8_t) calculatePadding(header.biWidth);

    for (uint32_t i = 0; i < header.biHeight; i++) {
        if (fread(img->data + i * header.biWidth, sizeof(struct pixel), header.biWidth, in) != header.biWidth) {
            return BMP_READ_INVALID_BITS;
        }
        if (fseek(in, padding, SEEK_CUR) != 0)
            return BMP_READ_INVALID_BITS;
    }


    return BMP_READ_OK;
}

static struct bmp_header create_header(struct image const* img, uint8_t padding) {
    struct bmp_header header = {0};
    header.bfType = SIGNATURE;
    
    header.bfileSize = sizeof(struct bmp_header) 
            + (size_t) img->width * (size_t) img->height * sizeof(struct pixel)
            + padding * (size_t) img->height;
    header.bfReserved = 0;
    header.bfOffBits = sizeof(struct bmp_header); 
    header.biSize = 40;
    header.biWidth = (uint32_t) img->width;
    header.biHeight = (uint32_t) img->height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biSizeImage = (uint32_t) (header.bfileSize - sizeof(struct bmp_header));
    header.biCompression = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
    header.biXPelsPerMeter = 0; 
    header.biYPelsPerMeter = 0;
    return header;
}



enum write_status to_bmp(FILE* const out, struct image * const img ) {
    if (!img) return BMP_WRITE_NULL_IMAGE;
    const uint8_t padding = (uint8_t) calculatePadding(img->width);
    const char paddingBytes[4] = {0};

    struct bmp_header header = create_header(img, padding);
    
    if(fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) return BMP_WRITE_ERROR;
    
    for (uint32_t i = 0; i < header.biHeight; i++) {
        for (uint32_t j = 0; j < header.biWidth; j++) {
            if (fwrite(img->data + (i * header.biWidth + j), 1, 3, out) != 3) {
                return BMP_WRITE_INVALID_BITS;
            }
        }
        if (fwrite(paddingBytes, 1, padding, out) != padding) {
            return BMP_WRITE_INVALID_BITS;
        }
    }
    return BMP_WRITE_OK;
}
