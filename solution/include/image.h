#pragma once
#include <stdint.h>

struct __attribute__((packed)) pixel {
	uint8_t b;
	uint8_t g;
	uint8_t r;
};

struct __attribute__((packed)) image {
  uint64_t width, height;
  struct pixel* data;
};


struct __attribute__((packed)) bmp_header 
{
        uint16_t bfType;
        uint32_t bfileSize;
        uint32_t bfReserved;
        uint32_t bfOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t biHeight;
        uint16_t biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t biClrImportant;
};

struct image create_image(uint64_t width, uint64_t height);

void delete_image(struct image *img);
