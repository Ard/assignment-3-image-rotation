#include "rotate.h"

struct image rotate(const struct image src) {
    struct image image = create_image(src.height, src.width); 
    if (image.data == NULL) return image; 
    for (size_t i = 0; i<src.width; i++) {  
        for (size_t j = 0; j<src.height; j++) {
            image.data[(i*src.height)+j] = src.data[i+(src.height - 1 -j)*src.width];
        }
    }
    return image;
}
