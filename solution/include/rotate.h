#pragma once

#include "image.h"
#include <inttypes.h>
#include <malloc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

struct image rotate(const struct image src);
