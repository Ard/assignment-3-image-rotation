#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

bool open_file(FILE** file, char* const filename, char* open_mode);
bool close_file(FILE* file);
